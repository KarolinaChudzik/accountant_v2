class Manager:

    def __init__(self):
        self.saldo = 0
        self.magazyn_dict = {}
        self.logs = []
        self.parametry = []
        self.cb = {} # call back funkcja dzialajaca w tle

    def read_input_data(self, file_path='input_data.txt'):
        with open(file_path) as file:
            for line in file.readlines():
                lines = line.split(",")
                lines = [x.strip() for x in lines]
                self.parametry.append(lines)

    def current_change(self, wartosc_zmiany, log):
        if wartosc_zmiany < 0 and wartosc_zmiany + self.saldo < 0:
            print("Niewystarczajace srodki!")
        else:
            self.saldo += wartosc_zmiany
            self.logs.append(log)

    def current_changes_purchases(self, index, price, number, log):
        wartosc_zakupu_koniec = number * price
        if wartosc_zakupu_koniec > self.saldo:
            print(f'Cena za towary ({wartosc_zakupu_koniec} '
                  f'przekracza wartosc salda {self.saldo}.)')
        else:
            self.saldo = self.saldo - wartosc_zakupu_koniec
            if not self.magazyn_dict.get(index):
                self.magazyn_dict[index] = {
                    "ilosc": number, "cena": price
                }
            else:
                magazyn_ilosc_prod = \
                    self.magazyn_dict[index]["ilosc"]
                self.magazyn_dict[index] = {
                    "ilosc": magazyn_ilosc_prod + number, "cena": price
                }
            self.logs.append(log)

    def current_changes_sales(self, index, price, number, log):
        wartosc_zmiany = price * number
        if not self.magazyn_dict.get(index):
            print("Brak produktu na stanie magazynowym, podaj inny produkt")
        else:
            if self.magazyn_dict.get(index)["ilosc"] < number:
                print("Brak wystarczajace ilosci produktow, "
                  "wprowadz inna ilosc.")
            magazyn_ilosc_prod = \
                self.magazyn_dict[index]["ilosc"]
            self.magazyn_dict[index] = {
                "ilosc": magazyn_ilosc_prod - number,
                "cena": price
            }
            self.saldo = self.saldo + wartosc_zmiany
            self.logs.append(log)
            if not self.magazyn_dict.get(index)['ilosc']:
                del self.magazyn_dict[index]

    def append_parametry(self, parametr):
        self.parametry.append(parametr)

    def save_parametry(self):
        with open("output_data", "w") as file:
            for item in self.parametry:
                file.write(str(item) + "\n")

    def assign(self, mode):
        def inner(func):    
            self.cb[mode] = func
        return inner

    def execute(self):
        for i in self.parametry:
            mode = i[0]
            if mode in self.cb:
                self.cb[mode](i[1:])
            elif mode == 'stop':
                print('Zakonczyles prace programu.')
            else:
                print(f'{mode} - nieobslugiwany rodzaj akcji.')
            

manager = Manager()

@manager.assign(mode='saldo')
def update_saldo(i):
    zmiana_salda = float(i[0])
    komentarz = i[1].replace("\n", "")
    log = f"Zmiana salda: {zmiana_salda} " \
            f"z komentarzem: {komentarz}."
    manager.current_change(zmiana_salda, log)

@manager.assign(mode='zakup')
def make_purchases(i):
        identyfikator_produktu_zakup = i[0]
        cena_jedn_zakup = int(i[1])
        ilosc_szt_zakup = int(i[2])
        log = f"Dokonano zakupu produktu {identyfikator_produktu_zakup}," \
                f"ilosc {ilosc_szt_zakup} w cenie za szt.{cena_jedn_zakup}."
        manager.current_changes_purchases(identyfikator_produktu_zakup,
                                        cena_jedn_zakup,
                                        ilosc_szt_zakup,
                                        log)

@manager.assign(mode='sprzedaż')
def make_sales(i): 
        identyfikator_produktu_sprzedaz = i[0]
        cena_jedn_sprzedaz = float(i[1])
        ilosc_szt_sprzedaz = int(i[2])
        log = f"Dokonano srzedazy " \
                f"produktu {identyfikator_produktu_sprzedaz}," \
                f"ilosc {ilosc_szt_sprzedaz} w cenie za szt. " \
                f"{cena_jedn_sprzedaz}."
        manager.current_changes_sales(identyfikator_produktu_sprzedaz,
                                    cena_jedn_sprzedaz,
                                    ilosc_szt_sprzedaz,
                                    log)