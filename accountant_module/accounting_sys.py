class AccountingSystem:

    def __init__(self):
        self.saldo = 0
        self.magazyn_dict = {}
        self.parametry = []
        self.logs = []

    def read_input_data(self, file_path):
        with open(file_path) as file:
            for line in file.readlines():
                lines = line.split(",")
                lines = [x.strip() for x in lines]
                self.parametry.append(lines)

    def append_parametry(self, parametr):
        self.parametry.append(parametr)

    def current_change(self, wartosc_zmiany, log):
        if wartosc_zmiany < 0 and wartosc_zmiany + self.saldo < 0:
            print("Niewystarczajace srodki!")
        else:
            self.saldo += wartosc_zmiany
            self.logs.append(log)

    def update_saldo(self):
        for i in self.parametry:
            if i[0] == "saldoc":
                wart_poczatkowa = float(i[1])
                komentarz = i[2].replace("\n", "")
                log = f"Zmiana saldo: {wart_poczatkowa} " \
                      f"z komentarzem: {komentarz}."
                self.current_change(wart_poczatkowa, log)
            if i[0] == "saldo":
                zmiana_salda = float(i[1])
                komentarz = i[2].replace("\n", "")
                log = f"Zmiana saldo: {zmiana_salda} " \
                      f"z komentarzem: {komentarz}."
                self.current_change(zmiana_salda, log)

    def current_changes_purchases(self, index, price, number, log):
        wartosc_zakupu_koniec = number * price
        if wartosc_zakupu_koniec > self.saldo:
            print(f'Cena za towary ({wartosc_zakupu_koniec} '
                  f'przekracza wartosc salda {self.saldo}.)')
            #continue
        else:
           self.saldo = self.saldo - wartosc_zakupu_koniec
        if not self.magazyn_dict.get(index):
            self.magazyn_dict[index] = {
                "ilosc": number, "cena": price
            }
        else:
            magazyn_ilosc_prod = \
                self.magazyn_dict[index]["ilosc"]
            self.magazyn_dict[index] = {
                "ilosc": magazyn_ilosc_prod + number, "cena": price
            }
        self.logs.append(log)

    def make_purchases(self):
        for i in self.parametry:
            if i[0] == "zakup":
                identyfikator_produktu_zakup = i[1]
                cena_jedn_zakup = int(i[2])
                ilosc_szt_zakup = int(i[3])
                log = f"Dokonano zakupu produktu {identyfikator_produktu_zakup}," \
                      f"ilosc {ilosc_szt_zakup} w cenie za szt.{cena_jedn_zakup}."
                self.current_changes_purchases(identyfikator_produktu_zakup,
                                               cena_jedn_zakup,
                                               ilosc_szt_zakup,
                                               log)

    def current_changes_sales(self, index, price, number, log):
        wartosc_zmiany = price * number
        if not self.magazyn_dict.get(index):
            print("Brak produktu na stanie magazynowym, podaj inny produkt")
        if self.magazyn_dict.get(index)["ilosc"] < number:
            print("Brak wystarczajace ilosci produktow, "
                  "wprowadz inna ilosc.")
        magazyn_ilosc_prod = \
            self.magazyn_dict[index]["ilosc"]
        self.magazyn_dict[index] = {
            "ilosc": magazyn_ilosc_prod - number,
            "cena": price
        }
        self.saldo = self.saldo + wartosc_zmiany
        self.logs.append(log)
        if not self.magazyn_dict.get(index)['ilosc']:
            del self.magazyn_dict[index]

    def make_sales(self):
        for i in self.parametry:
            if i[0] == "sprzedaz":
                identyfikator_produktu_sprzedaz = i[1]
                cena_jedn_sprzedaz = float(i[2])
                ilosc_szt_sprzedaz = int(i[3])
                log = f"Dokonano srzedazy " \
                      f"produktu {identyfikator_produktu_sprzedaz}," \
                      f"ilosc {ilosc_szt_sprzedaz} w cenie za szt. " \
                      f"{cena_jedn_sprzedaz}."
                self.current_changes_sales(identyfikator_produktu_sprzedaz,
                                           cena_jedn_sprzedaz,
                                           ilosc_szt_sprzedaz,
                                           log)

    def warehouse_change(self, magazyn):
        for i in magazyn:
            self.magazyn_dict[i] = {
                "ilosc": 0,
                "cena": 0
            }

    def system_closed(self):
        for i in self.parametry:
            if i[0] == "stop":
                print("Zakonczyles prace programu!")
                break

    def save_parametry(self):
        with open("output_data", "w") as file:
            for item in self.parametry:
                file.write(str(item) + "\n")

    def save_logs(self):
        with open("output_data", "w") as file:
            for item in self.logs:
                file.write(str(item) + str("\n"))