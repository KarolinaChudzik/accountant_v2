import sys
from acc_manager import manager


file_name = sys.argv[1]
identyfikator_produktu = sys.argv[2:]

manager.read_input_data(file_name)
manager.execute()

print(manager.magazyn_dict)
for index in identyfikator_produktu:
    if not index in manager.magazyn_dict:
        print("{}: ilosc 0, cena 0".format(index))
    else:
        print("{}: ilosc {}, cena {}".format(index,
                                         manager.magazyn_dict[index]["ilosc"],
                                         manager.magazyn_dict[index]["cena"]))