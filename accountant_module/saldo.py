import sys
from acc_manager import manager

file_name = sys.argv[1]
wartosc_saldo = float(sys.argv[2])
komentarz_saldo = sys.argv[3]

manager.read_input_data(file_name)
manager.execute()

log = f"Zmiana saldo: {wartosc_saldo} z komentarzem: {komentarz_saldo}."
manager.current_change(wartosc_saldo, log)
paramert = ["saldo, {}, {}".format(wartosc_saldo, komentarz_saldo)]
manager.append_parametry(paramert)
print(manager.parametry)
manager.save_parametry()