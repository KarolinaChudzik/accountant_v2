import sys
from acc_manager import manager

file_name = sys.argv[1]
identyfikator_produktu = sys.argv[2]
cena_jednostkowa = int(sys.argv[3])
ilosc_szt = int(sys.argv[4])

manager.read_input_data(file_name)
manager.execute()


log = f"Dokonano sprzedazy produktu: {identyfikator_produktu}, ilosc:" \
      f" {ilosc_szt}, w cenie za szt.  {cena_jednostkowa}."
paramert = ["sprzedaz, {}, {}, {}".format(identyfikator_produktu,
                                          cena_jednostkowa,
                                          ilosc_szt)]
manager.append_parametry(paramert)
manager.current_changes_sales(
      identyfikator_produktu,
      cena_jednostkowa,
      ilosc_szt,
      log)
print(manager.parametry)
manager.save_parametry()
