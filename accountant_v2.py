import sys

from accountant_module.cons import DOZWOLONE_WYWOLANIA, LISTA_DOZWOLONYCH_AKCJI


komendy = sys.argv[1]
saldo = 0
magazyn_dict = {}
parametry = []
logs = []

with open("input_data") as input_:
    for line in input_.readlines():
        lines = line.split(",")
        akcja_rodzaj = lines[0]
        if not akcja_rodzaj in LISTA_DOZWOLONYCH_AKCJI:
            print("Niedozwolona komenda, podaj prawidlowa komende.")
            continue
        elif akcja_rodzaj == "stop":
            print("Zakonczyles prace systemu!")
            break
        elif akcja_rodzaj == "saldoc":
            wart_pocz = float(lines[1])
            saldo += wart_pocz
        elif akcja_rodzaj == "saldo":
            konto_zmiana_saldo = float(lines[1])
            zmiana_komentarz_saldo = lines[2].replace("\n", "")
            paratmert = ("Rodzaj akcji:{} "
                         "Wartosc zmiany na koncie (gr):{}. "
                         "Komentarz do zmiany: {}. ".
                         format(akcja_rodzaj, konto_zmiana_saldo,
                                zmiana_komentarz_saldo))
            parametry.append(paratmert)
            if (konto_zmiana_saldo) < 0 and (saldo + konto_zmiana_saldo) < 0:
                print("Podana wartosc przewyzsza saldo, wpisz wartosc ponownie.")
                continue
            saldo = saldo + konto_zmiana_saldo
            log = f"Zmiana saldo: {konto_zmiana_saldo} " \
                  f"z komentarzem: {zmiana_komentarz_saldo}."
            logs.append(log)
        elif akcja_rodzaj == "zakup":
            identyfikator_produktu_zakup = lines[1]
            cena_jedn_zakup = float(lines[2])
            ilosc_szt_zakup = int(lines[3].replace(".\n", ""))
            paratmert = ("Rodzaj akcji: {} "
                         "Identyfikator produktu:{}. "
                         "Cene jednostkowa: {}. "
                         "Ilosc szt:{}".
                         format(akcja_rodzaj, identyfikator_produktu_zakup,
                                cena_jedn_zakup, ilosc_szt_zakup))
            parametry.append(paratmert)
            wartosc_zakupu = ilosc_szt_zakup * cena_jedn_zakup
            if wartosc_zakupu > saldo:
                print(f'Cena za towary ({wartosc_zakupu} '
                      f'przekracza wartosc salda {saldo}.)')
                continue
            else:
                saldo = saldo - wartosc_zakupu
                if not magazyn_dict.get(identyfikator_produktu_zakup):
                    magazyn_dict[identyfikator_produktu_zakup] = {
                        "ilosc": ilosc_szt_zakup, "cena": cena_jedn_zakup
                    }
                else:
                    magazyn_ilosc_prod = magazyn_dict[identyfikator_produktu_zakup]["ilosc"]
                    magazyn_dict[identyfikator_produktu_zakup] = {
                        "ilosc": magazyn_ilosc_prod + ilosc_szt_zakup,
                        "cena": cena_jedn_zakup
                    }
            log = f"Dokonano zakupu produktu {identyfikator_produktu_zakup}," \
                  f"ilosc {ilosc_szt_zakup} w cenie za szt.{cena_jedn_zakup}."
            logs.append(log)
        elif akcja_rodzaj == "sprzedaz":
            identyfikator_produktu_sprzedaz = lines[1]
            cena_jedn_sprzedaz = float(lines[2])
            ilosc_szt_sprzedaz = int(lines[3].replace("\n", ""))
            paratmert = ("Rodzaj akcji: {} "
                         "Identyfikator produktu:{}. "
                         "Cene jednostkowa: {}. "
                         "Ilosc szt:{}".
                         format(akcja_rodzaj, identyfikator_produktu_sprzedaz,
                                cena_jedn_sprzedaz, ilosc_szt_sprzedaz))
            parametry.append(paratmert)
            wartosc_sprzedazy = cena_jedn_sprzedaz * ilosc_szt_sprzedaz
            if not magazyn_dict.get(identyfikator_produktu_sprzedaz):
                print("Brak produktu na stanie magazynowym, podaj inny produkt")
                continue
            if magazyn_dict.get(identyfikator_produktu_sprzedaz)["ilosc"] < \
                    ilosc_szt_sprzedaz:
                print("Brak wystarczajace ilosci produktow, "
                      "wprowadz inna ilosc.")
                continue
            magazyn_ilosc_prod = \
                magazyn_dict[identyfikator_produktu_sprzedaz]["ilosc"]
            magazyn_dict[identyfikator_produktu_sprzedaz] = {
                "ilosc": magazyn_ilosc_prod - ilosc_szt_sprzedaz,
                "cena": cena_jedn_sprzedaz
            }
            saldo = saldo + wartosc_sprzedazy
            if not magazyn_dict.get(identyfikator_produktu_sprzedaz)['ilosc']:
                del magazyn_dict[identyfikator_produktu_sprzedaz]
            log = f"Dokonano srzedazy produktu " \
                  f"{identyfikator_produktu_sprzedaz}," \
                  f"ilosc {ilosc_szt_sprzedaz} w cenie za szt." \
                  f"{cena_jedn_sprzedaz}."
            logs.append(log)


if komendy == "konto":
    print(f"Saldo:{saldo}")
    output = open("output_data", "w")
    output.write("Saldo: " + str(saldo))
    output.close()
elif komendy == "magazyn":
    magazyn = (sys.argv[2:])
    ilosc = magazyn_dict.get("ilosc")
    cena = magazyn_dict.get("cena")
    print(ilosc)
    for i in magazyn:
        magazyn_dict[i] = {
            "ilosc": 0,
            "cena": 0
        }
    print(f"Stan magazynu:{magazyn_dict}")
    with open("output_data", "w") as file:
        for line, value in magazyn_dict.items():
            file.write(str(line) + str(value) + "\n")
elif komendy == "przeglad":
    print(logs)
    with open("output_data", "w") as f:
        for item in logs:
            f.write("%s\n" % item)
elif komendy == "saldo":
    wartosc_zmiany = int(sys.argv[2])
    komentarz = sys.argv[3]
    if (wartosc_zmiany) < 0 and (saldo + wartosc_zmiany) < 0:
        print("Podana wartosc przewyzsza saldo, wpisz wartosc ponownie.")
    saldo = saldo + wartosc_zmiany
    paratmert = ("Rodzaj akcji: {} "
                 "Wartosc zmiany na koncie (gr):{}. "
                 "Komentarz do zmiany: {}. ".
                 format(sys.argv[1], wartosc_zmiany, komentarz))
    with open("output_data", "w") as f:
        for item in parametry:
            f.write(str(item) + "\n")
elif komendy == "sprzedaz":
    identyfikator_sprzedaz = sys.argv[2]
    cena_sprzedaz = int(sys.argv[3])
    ilosc_sprzedaz = int(sys.argv[4])
    paratmert = ("Rodzaj akcji: {} "
                 "Identyfikator produktu: {}. "
                 "Cene jednostkowa: {}. "
                 "Ilosc szt:{}".
                 format(sys.argv[1], identyfikator_sprzedaz,
                        cena_sprzedaz, ilosc_sprzedaz))
    parametry.append(paratmert)
    wartosc_sprzedazy_koniec = cena_sprzedaz * ilosc_sprzedaz
    if not magazyn_dict.get(identyfikator_sprzedaz) or \
            magazyn_dict.get(identyfikator_sprzedaz)["ilosc"] < ilosc_sprzedaz:
        print("Brak produktu na stanie lub niewystrczajaca ilosc produktow, "
              "aby dokonac sprzedazy.")
    magazyn_dict[identyfikator_sprzedaz] = {
        "ilosc": magazyn_ilosc_prod - ilosc_sprzedaz,
        "cena": cena_sprzedaz
    }
    saldo = saldo + wartosc_sprzedazy_koniec
    if not magazyn_dict.get(identyfikator_sprzedaz)['ilosc']:
        del magazyn_dict[identyfikator_sprzedaz]
    with open("output_data", "w") as f:
        for item in parametry:
            f.write(str(item) + "\n")

elif komendy == "zakup":
    identyfikator_zakup = str(sys.argv[2])
    cena_zakup = int(sys.argv[3])
    ilosc_zakup = int(sys.argv[4])
    wartosc_zakupu_koniec = ilosc_zakup * cena_zakup
    paratmert = ("Rodzaj akcji: {} "
                 "Identyfikator produktu:{}. "
                 "Cene jednostkowa: {}. "
                 "Ilosc szt:{}".
                 format(sys.argv[1], identyfikator_zakup,
                        cena_zakup, ilosc_zakup))
    parametry.append(paratmert)
    if wartosc_zakupu_koniec > saldo:
        print(f'Cena za towary ({wartosc_zakupu_koniec} '
              f'przekracza wartosc salda {saldo}.)')
    else:
        saldo = saldo - wartosc_zakupu_koniec
    if not magazyn_dict.get(identyfikator_zakup):
        magazyn_dict[identyfikator_zakup] = {"ilosc": ilosc_zakup,
                                              "cena": cena_zakup}
    else:
        magazyn_ilosc_prod = magazyn_dict[identyfikator_zakup]["ilosc"]
        magazyn_dict[identyfikator_zakup] = {
            "ilosc": magazyn_ilosc_prod + ilosc_zakup,"cena": cena_zakup
        }
    with open("output_data", "w") as f:
        for item in parametry:
            f.write(str(item) + "\n")

for podane_parametry in enumerate(parametry):
    if komendy in DOZWOLONE_WYWOLANIA:
        print(f"Podane parametry:{podane_parametry}")